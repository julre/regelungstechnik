import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;

/**
 *  manages the Tree image
 * @author Lukas
 *
 */
public class Tree 
{
	/**
	 * the brown tree image
	 */
	private Image tree;
	/**
	 * the green crown of a tree, also a image
	 */
	private Image crown;
	
	/**
	 * is visible?
	 */
	private boolean visible;
	
	/**
	 * position X of the tree.
	 */
	private int posX;
	/**
	 * position Y of the tree.
	 */
	private int posY;
	
	/**
	 * default constructor.
	 * sets the position values to X:0 Y:5 and loads the image into the variables
	 * @param app
	 */
	public Tree(Applet app)
	{
		posX = 0;
		posY = 5;
		try
		{
			tree = app.getImage(app.getDocumentBase(), "res/others/tree.PNG");
			crown = app.getImage(app.getDocumentBase(), "res/others/tree2.PNG");
		}
		catch(Exception e)
		{
			System.err.println("Couldnt load the Images");
		}	
	}
	
	/**
	 * getter for the visiblility of the tree
	 * @return true if visible, false if not visible
	 */
	public boolean isVisible() 
	{
		return visible;
	}
	
	/**
	 * setter for the visibility of the tree
	 * @param visible
	 */
	public void setVisible(boolean visible) 
	{
		this.visible = visible;
	}
	
	/**
	 * getter for Position X
	 * @return the x - position of the tree
	 */
	public int getPosX() 
	{
		return posX;
	}
	
	/**
	 * setter for Position X
	 * @param posX
	 */
	public void setPosX(int posX) 
	{
		this.posX = posX;
	}
	
	/** 
	 * getter for Position Y
	 * @return the y - position of the tree
	 */
	public int getPosY() 
	{
		return posY;
	}
	
	/**
	 * setter for Position Y
	 * @param posY
	 */
	public void setPosY(int posY) 
	{
		this.posY = posY;
	}
	
	/**
	 * moves the tree around the x - axi
	 */
	public void move(int celerity)
	{
		posX += celerity/12;
	}
	
	/**
	 * paints the tree
	 * @param g the graphic device
	 * @param app the applet device
	 */
	public void paint(Graphics g, Applet app)
	{
		g.drawImage(tree, posX, posY, app);
		g.drawImage(crown, posX-15, posY-18, app);
	}

}
