/**
 * 
 */
package de.fhswf.regelungstechnik.panels;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import de.fhswf.regelungstechnik.JSliderCarCelerity;
import de.fhswf.regelungstechnik.model.AbstandsregelungModel;

/**
 * Panal with an border layout for a slider in the center and a laber in the
 * north.
 * 
 * @author Julian Rehborn
 */
public class JPanelCelerityControl extends JPanel {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor which sets the layout and the positions.
   * 
   * @param model
   *          for the constructor for the {@link JSliderCarCelerity}.
   */
  public JPanelCelerityControl(AbstandsregelungModel model) {
    setLayout(new BorderLayout());
    add(new JLabel("km/h"), BorderLayout.NORTH);
    add(new JSliderCarCelerity(model), BorderLayout.CENTER);
  }
}
