/**
 * 
 */
package de.fhswf.regelungstechnik.panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import de.fhswf.regelungstechnik.Main;
import de.fhswf.regelungstechnik.complexgraphics.Blockschaltbild;
import de.fhswf.regelungstechnik.complexgraphics.Blockstrukturbild;
import de.fhswf.regelungstechnik.model.AbstandsregelungModel;
import de.fhswf.regelungstechnik.model.Tree;

/**
 * @author root
 */
public class JPanelVisualisation extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 7694758946776095902L;
  private AbstandsregelungModel model;

  /**
   * Trees to visualize the celerity.
   */
  private Tree[] trees;

  /**
   * The image of the pickup.
   */
  private Image frontCarImg;

  /**
   * The image of the ferrari.
   */
  private Image backCarImg;

  /**
   * The image of the street.
   */
  private Image street;

  /**
   * Draws the blockdiagram.
   */
  private Blockschaltbild blockdiagram;

  /**
   * Draws the blockstructurediagram.
   */
  private Blockstrukturbild blockstrukturbild;

  private Main app;

  public JPanelVisualisation(AbstandsregelungModel model, Main app) {
    this.model = model;
    this.app = app;

    blockdiagram = new Blockschaltbild();
    blockstrukturbild = new Blockstrukturbild();

    // load graphics.
    try {
      trees = new Tree[2];

      street = app.getImage(app.getDocumentBase(), "res/others/street.PNG");
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println("Couldnt load the Images");
    }
    trees[0] = new Tree(app);
    trees[1] = new Tree(app);
    trees[1].setPosX(350);

  }

  /**
   * Funktion to draw distance lines.
   * 
   * @param g
   *          Graphic variable
   * @param text
   *          text which appears of the line
   * @param startX
   * @param startY
   * @param endX
   * @param endY
   */
  private void drawDistance(Graphics g, String text, int startX, int startY,
      int endX, int endY) {
    // horizontal line.
    g.drawLine(startX, startY, endX, endY);

    // diagonal line.
    g.drawLine(startX, startY - 10, startX, startY + 10);
    g.drawLine(endX, endY - 10, endX, endY + 10);

  };

  public void moveTrees() {
    trees[0].move((int) (model.getFrontCar().getCelerity() * 1.0));
    trees[1].move((int) (model.getFrontCar().getCelerity() * 1.0));
  }

  private void offPaint(Graphics g) {

    // set the animation graphic for the cars.
    try {
      frontCarImg = app.getImage(app.getDocumentBase(), model.getFrontCar()
          .getPathImage(model.getDoRun()));
      backCarImg = app.getImage(app.getDocumentBase(), model.getBackCar()
          .getPathImage(model.getDoRun()));
    } catch (Exception e) {
      System.err.println("Couldnt load car images");
    }

    // draw street.
    g.drawImage(street, 0, 0, 710, 213, this);

    // draw pickup.
    g.drawImage(frontCarImg, model.getFrontCar().getFrontX(), model
        .getFrontCar().getFrontY(), this);

    // draw ferrari.
    g.drawImage(backCarImg, model.getBackCar().getFrontX(), model.getBackCar()
        .getFrontY(), this);

    // draw background and short the street-graphic.
    g.setColor(new Color(238, 238, 238));
    g.fillRect(0, 135, 710, 813);
    g.setColor(Color.black);

    // draw reference distance label.
    g.drawString("Sollabstand: " + model.getReferenceDistance() + "m", 200, 18);
    g.setColor(new Color(255, 0, 0));

    // draw reference distance visualization.
    drawDistance(g,
        "Sollabstand: " + String.valueOf(model.getReferenceDistance()) + "m",
        model.getBackCar().getFrontX() - model.getBackCar().getCelerity() / 2
            * model.getMeterScalingfactor(), model.getBackCar().getPosY() - 10,
        model.getBackCar().getFrontX(), model.getBackCar().getPosY() - 10);

    // draw current distance label.
    g.setColor(Color.black);
    g.drawString(
        "Istabstand : " + -model.getCurrentDistance()
            / model.getMeterScalingfactor() + "m", 200, 43);

    // choose color for current distance
    if (model.getCurrentDistance() / model.getMeterScalingfactor() < model
        .getReferenceDistance() * 1.05
        && model.getCurrentDistance() / model.getMeterScalingfactor() > model
            .getReferenceDistance() * 0.95)
      g.setColor(new Color(0, 255, 0));
    if (model.getCurrentDistance() / model.getMeterScalingfactor() > model
        .getReferenceDistance() * 1.05)
      g.setColor(new Color(155, 155, 155));
    if (model.getCurrentDistance() / model.getMeterScalingfactor() < model
        .getReferenceDistance() * 0.95)
      g.setColor(new Color(151, 33, 255));

    // draw current distance visualization.
    drawDistance(g, "Istabstand: " + String.valueOf(model.getCurrentDistance())
        + "m", model.getFrontCar().getBackX(),
        model.getFrontCar().getMiddleY(), model.getBackCar().getFrontX(), model
            .getBackCar().getMiddleY());
    g.setColor(Color.black);

    // draw blockdiagram.
    if (model.getDrawBlockdiagram())
      blockdiagram.paint(g,
          model.getFrontCar().getCelerity(), //
          -model.getCurrentDistance() / model.getMeterScalingfactor(), model
              .getReferenceDistance(),
          model.getBackCar().isDistanceControlON(), app.getRegulator()
              .getStatus(), 140);

    // draw controllloop.
    if (model.getDrawControlLoop()) {
      int yOffset = 140;
      // draw on the bottom if blockdiagram is enabled
      if (model.getDrawBlockdiagram())
        yOffset += 285;
      blockstrukturbild.paint(g, model.getBackCar().isDistanceControlON(),
          model.getBackCar().isDistanceControlON(), model.getBackCar()
              .getCelerity(),
          -model.getCurrentDistance() / model.getMeterScalingfactor(), model
              .getReferenceDistance(), yOffset);
    }

    // paint trees.
    trees[0].paint(g, app);
    trees[1].paint(g, app);
    if (trees[0].getPosX() > 700)
      trees[0].setPosX(0);
    if (trees[1].getPosX() > 700)
      trees[1].setPosX(0);

  }

  @Override
  public void paint(Graphics g) {

    super.paint(g);
    offPaint(g);
    System.out.println("lol");
    System.out.println(g.toString());
  }

  @Override
  public void update(Graphics g) {
    paint(g);
    System.out.println("update");
  }

}
