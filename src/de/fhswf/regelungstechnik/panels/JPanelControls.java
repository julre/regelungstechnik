/**
 * 
 */
package de.fhswf.regelungstechnik.panels;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import de.fhswf.regelungstechnik.actions.ActionDistanceControl;
import de.fhswf.regelungstechnik.actions.ActionDrawBlockdiagram;
import de.fhswf.regelungstechnik.actions.ActionDrawControlLoop;
import de.fhswf.regelungstechnik.actions.ActionStartStop;
import de.fhswf.regelungstechnik.model.AbstandsregelungModel;

/**
 * Panel for the controllbutton on the top of the applet.
 * 
 * @author Julian Rehborn
 */
public class JPanelControls extends JPanel {

  private static final long serialVersionUID = 1L;

  /**
   * The model.
   */
  private AbstandsregelungModel model;

  /**
   * Default constructor for this panel.
   * Creates all buttons and creates the actions.
   * 
   * @param model
   *          the model.
   */
  public JPanelControls(AbstandsregelungModel model) {
    this.model = model;
    setLayout(new GridLayout());

    add(new JButton(new ActionStartStop("Start", "Stop", this.model)));
    add(new JButton(new ActionDistanceControl("Regelung ausschalten",
        "Regelung anschalten", this.model)));
    add(new JButton(new ActionDrawBlockdiagram("Blockschaltbild An",
        "Blockschaltbild Aus", this.model)));
    add(new JButton(new ActionDrawControlLoop("Regelungskreis An",
        "Regelungskreis Aus", this.model)));
  }
}
