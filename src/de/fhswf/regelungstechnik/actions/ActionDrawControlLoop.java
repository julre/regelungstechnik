/**
 * 
 */
package de.fhswf.regelungstechnik.actions;

import java.awt.event.ActionEvent;

import de.fhswf.regelungstechnik.model.AbstandsregelungModel;

/**
 * Action to enable/disable the blockdiagram.
 * 
 * @author Julian Rehborn
 */
public class ActionDrawControlLoop extends ActionCommon {

  private static final long serialVersionUID = 1L;

  /**
   * Model of the program.
   */
  private AbstandsregelungModel model;

  /**
   * Constructor for this action.
   * 
   * @param activText
   *          sets the active text.
   * @param inactivText
   *          sets the inactive text.
   * @param model
   *          sets the model.
   */
  public ActionDrawControlLoop(String activText, String inactivText,
      AbstandsregelungModel model) {
    super(activText, inactivText, !model.getDrawControlLoop());
    this.model = model;
  }

  /*
   * (non-Javadoc)
   * @see
   * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent arg0) {
    model.setDrawControlLoop(!model.getDrawControlLoop());
    super.actionPerformed(arg0);
  }
}
