/**
 * 
 */
package de.fhswf.regelungstechnik.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import de.fhswf.regelungstechnik.model.AbstandsregelungModel;

/**
 * Action to enabele/disable the distance controll.
 * 
 * @author Julian Rehborn
 */
public class ActionDistanceControl extends ActionCommon {

  private static final long serialVersionUID = 1L;

  /**
   * Model of the program.
   */
  AbstandsregelungModel model;

  /**
   * Constructor for this action.
   * 
   * @param activText
   *          sets the active text.
   * @param inactivText
   *          sets the inactive text.
   * @param model
   *          sets the model.
   */
  public ActionDistanceControl(String activText, String inactivText,
      AbstandsregelungModel model) {
    super(activText, inactivText, model.getBackCar().isDistanceControlON());

    this.model = model;
  }

  /*
   * (non-Javadoc)
   * @see
   * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent arg0) {

    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        model.getBackCar().switchDistanceControl();
      }
    });

    super.actionPerformed(arg0);
  }
}
