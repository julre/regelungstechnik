/**
 * 
 */
package de.fhswf.regelungstechnik.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Baseclass for all used actions.
 * 
 * @author Julian Rehborn
 */
public class ActionCommon extends AbstractAction {

  private static final long serialVersionUID = 1L;
  private Boolean status;

  /**
   * Text for disabled option.
   */
  private String inactivText;

  /**
   * Text for enabled option.
   */
  private String activText;

  /**
   * Constructor to the set all attributes.
   * 
   * @param activText
   *          the activeText to set.
   * @param inactivText
   *          the inactiveText to set.
   * @param initStatus
   *          the initStatus to set.
   */
  public ActionCommon(String activText, String inactivText, Boolean initStatus) {

    status = initStatus;
    this.activText = activText;
    this.inactivText = inactivText;

    setText();

  }

  /*
   * (non-Javadoc)
   * @see
   * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent arg0) {
    status = !status;
    setText();
  }

  /**
   * Sets the texts.
   */
  private void setText() {
    if (status)
      putValue(Action.NAME, activText);
    else
      putValue(Action.NAME, inactivText);
  }
}
