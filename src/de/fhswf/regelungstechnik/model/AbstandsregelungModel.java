/**
 * 
 */
package de.fhswf.regelungstechnik.model;

/**
 * Hold all data.
 * 
 * @author Julian Rehborn, Andreas Michel
 */
public class AbstandsregelungModel {

  /**
   * If true the blockdiagram should be painted.
   */
  private Boolean drawBlockdiagram;

  /**
   * If true the controll loop should be painted.
   */
  private Boolean drawControlLoop;

  /**
   * If true the animation should be painted.
   */
  private Boolean drawAnimation;

  /**
   * If true the simulation should run.
   */
  private Boolean doRun;

  /**
   * Saclingfactor for the simulation.
   */
  private int meterScalingfactor;

  /**
   * The first car.
   */
  private Car frontCar;

  /**
   * the second car.
   */
  private Car backCar;

  /**
   * Default constructor.
   * Sets all attributs to default values.
   */
  public AbstandsregelungModel() {
    drawBlockdiagram = false;
    drawControlLoop = false;
    doRun = false;
    drawAnimation = true;
    meterScalingfactor = 12;
    int FrontCarInitX = 50;
    int PositionY = 30;

    backCar = new Car("ferrari");
    backCar.setCelerity(50);
    backCar
        .setPosX(FrontCarInitX + meterScalingfactor * getReferenceDistance());
    backCar.setPosY(PositionY);

    frontCar = new Car("pickup", FrontCarInitX, PositionY);
  }

  /**
   * @return the backCar
   */
  public Car getBackCar() {
    return backCar;
  }

  /**
   * @return the current distance. frontCar.getBack() - backCar.getFront()
   */
  public int getCurrentDistance() {
    return frontCar.getBackX() - backCar.getFrontX();
  }

  /**
   * @return the doRun
   */
  public Boolean getDoRun() {
    return doRun;
  }

  /**
   * @return the drawAnimation
   */
  public Boolean getDrawAnimation() {
    return drawAnimation;
  }

  /**
   * @return the drawBlockdiagram
   */
  public Boolean getDrawBlockdiagram() {
    return drawBlockdiagram;
  }

  /**
   * @return the drawControlLoop
   */
  public Boolean getDrawControlLoop() {
    return drawControlLoop;
  }

  /**
   * @return the frontCar
   */
  public Car getFrontCar() {
    return frontCar;
  }

  /**
   * @return the meterScalingfactor
   */
  public int getMeterScalingfactor() {
    return meterScalingfactor;
  }

  /**
   * @return the ReferanceDistance.
   */
  public int getReferenceDistance() {
    return backCar.getCelerity() / 2;
  }

  /**
   * @param backCar
   *          the backCar to set
   */
  public void setBackCar(Car backCar) {
    this.backCar = backCar;
  }

  /**
   * @param doRun
   *          the doRun to set
   */
  public void setDoRun(Boolean doRun) {
    this.doRun = doRun;
  }

  /**
   * @param drawAnimation
   *          the drawAnimation to set
   */
  public void setDrawAnimation(Boolean drawAnimation) {
    this.drawAnimation = drawAnimation;
  }

  /**
   * @param drawBlockdiagram
   *          the drawBlockdiagram to set
   */
  public void setDrawBlockdiagram(Boolean drawBlockdiagram) {
    this.drawBlockdiagram = drawBlockdiagram;
  }

  /**
   * @param drawControlLoop
   *          the drawControlLoop to set
   */
  public void setDrawControlLoop(Boolean drawControlLoop) {
    this.drawControlLoop = drawControlLoop;
  }

  /**
   * @param frontCar
   *          the frontCar to set
   */
  public void setFrontCar(Car frontCar) {
    this.frontCar = frontCar;
  }

  /**
   * @param meterScalingfactor
   *          the meterScalingfactor to set
   */
  public void setMeterScalingfactor(int meterScalingfactor) {
    this.meterScalingfactor = meterScalingfactor;
  }

}
