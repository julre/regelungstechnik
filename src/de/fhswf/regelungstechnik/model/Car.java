package de.fhswf.regelungstechnik.model;

/**
 * Class to hold any car information.
 * 
 * @author Lukas Kopecki
 * @author Julian Rehborn
 * @author Andreas Michel this class manages the cars
 */
public class Car {
  public enum DistanceControlStatus {
    ON, OFF
  }

  /**
   * X Position of the car
   */
  private int posX;
  /**
   * Y Position of the car
   */
  private int posY;
  /**
   * the middle X positon of the car for the drawing box
   */
  private int middleX;
  /**
   * the middle Y positon of the car for the drawing box
   */
  private int middleY;
  /**
   * the front X positon of the car for the drawing box
   */
  private int frontX;
  /**
   * the front Y positon of the car for the drawing box
   */
  private int frontY;
  /**
   * the back X positon of the car for the drawing box
   */
  private int backX;

  /**
   * the back X positon of the car for the drawing box
   */
  private int backY;
  /**
   * says when the car has to jostle the other one
   */
  private boolean jostle;

  /**
   * is the car to neara?
   */
  private boolean toNear;

  /**
   * frame switcher
   */
  private int frameSwitcher;

  /**
   * celerity of the car
   */
  private int celerity;

  /**
   * old celerity of the other car
   */
  private int oldcelerity;

  /**
   * car image
   */
  private String[] carImage = new String[2];

  /**
   * movement with regegelung = off with frequenz (0.2 Hz)
   */
  private boolean movement;

  /**
   * old sollabstand value
   */
  private int oldSollabstandValue;

  /**
   * counter for the animation
   */
  private int animationCounter;

  /**
   * summ of all differences between current position and new position
   */
  private int esum;

  private String name;

  private DistanceControlStatus distanceControl;

  /**
   * default constructor sets all to default values
   */
  public Car(String name) {
    this.name = name;
    distanceControl = DistanceControlStatus.OFF;
    posX = 0;
    posY = 0;
    celerity = 50;
    oldcelerity = 0;
    oldSollabstandValue = 0;
    jostle = false;
    esum = 0;
    String path = "res/cars/";
    carImage[0] = path + name.toLowerCase() + "1.PNG";
    carImage[1] = path + name.toLowerCase() + "2.PNG";
  }

  /**
   * Posistion constructor sets up the posistion of the Image
   * 
   * @param posX
   *          x-position
   * @param posY
   *          y-position
   */
  public Car(String name, int posX, int posY) {
    this(name);
    // sets position
    this.posX = posX;
    this.posY = posY;

    // sets box positions
    setFrontX(posX);
    setFrontY(posY);
    setMiddleX(posX + 32);
    setMiddleY(posY + 16);
    setBackX(posX + 64);
    setBackY(posY + 32);
  }

  public void disabledistanceControl() {
    distanceControl = DistanceControlStatus.OFF;
  }

  public void enabledistanceControl() {
    distanceControl = DistanceControlStatus.ON;
  }

  /**
   * @return the animationCounter
   */
  public int getAnimationCounter() {
    return animationCounter;
  }

  /**
   * getter for the back x position
   * 
   * @return int the back X position
   */
  public int getBackX() {
    return backX;
  }

  /**
   * setter for the back Y position
   * 
   * @return int the back Y position
   */
  public int getBackY() {
    return backY;
  }

  /**
   * getter function for the celerity
   * 
   * @return returns the celerity
   */
  public int getCelerity() {
    return celerity;
  }

  /**
   * @return the distanceControl
   */
  public DistanceControlStatus getDistanceControl() {
    return distanceControl;
  }

  /**
   * @return the esum
   */
  public int getEsum() {
    return esum;
  }

  /**
   * getter for the front X position
   * 
   * @return the front x position
   */
  public int getFrontX() {
    return frontX;
  }

  /**
   * getter for the front Y position
   * 
   * @return int the front Y position
   */
  public int getFrontY() {
    return frontY;
  }

  /**
   * gets the middle x value
   * 
   * @return the middle X position
   */
  public int getMiddleX() {
    return middleX;
  }

  /**
   * gets the middle Y
   * 
   * @return the middle Y position
   */
  public int getMiddleY() {
    return middleY;
  }

  public String getName() {
    return name;
  }

  /**
   * @return the oldcelerity
   */
  public int getOldcelerity() {
    return oldcelerity;
  }

  /**
   * @return the oldSollabstandValue
   */
  public int getOldSollabstandValue() {
    return oldSollabstandValue;
  }

  /**
   * Toggles the returnd imagepath.
   * 
   * @param doRun
   *          If doRun is true, the returned path is the first in every
   *          case.
   * @return the path of the image.
   */
  public String getPathImage(boolean doRun) {
    if (!doRun)
      return carImage[0];
    if (frameSwitcher == 0)
      frameSwitcher = 1;
    else
      frameSwitcher = 0;

    return carImage[frameSwitcher];
  }

  /**
   * getter function for PosX
   * 
   * @return the x-position
   */
  public int getPosX() {
    return posX;
  }

  /**
   * getter function for the PosY
   * 
   * @return the y-postion
   */
  public int getPosY() {
    return posY;
  }

  /**
   * @return true if the distance controll is on.
   */
  public Boolean isDistanceControlON() {
    Boolean on;
    if (distanceControl == DistanceControlStatus.ON)
      on = true;
    else
      on = false;
    return on;
  }

  /**
   * getter if the car ist jostle
   * 
   * @return true if the car is jostling the other one, false he does nothing
   */
  public boolean isJostle() {
    return jostle;
  }

  /**
   * @return the movement
   */
  public boolean isMovement() {
    return movement;
  }

  /**
   * setter if the car is to near
   * 
   * @return if the car is to near to the other one
   */
  public boolean isToNear() {
    return toNear;
  }

  /**
   * @param animationCounter
   *          the animationCounter to set
   */
  public void setAnimationCounter(int animationCounter) {
    this.animationCounter = animationCounter;
  }

  /**
   * setter for the Back X position
   * 
   * @param backX
   *          the back X position
   */
  private void setBackX(int backX) {
    this.backX = backX;
  }

  /**
   * setter for the Back Y position
   * 
   * @param backY
   *          the back Y position
   */
  private void setBackY(int backY) {
    this.backY = backY;
  }

  /**
   * setter function for the celerity
   * 
   * @param celerity
   *          the new celerity
   */
  public void setCelerity(int celerity) {
    this.celerity = celerity;
  }

  /**
   * @param distanceControl
   *          the distanceControl to set
   */
  public void setDistanceControl(DistanceControlStatus distanceControl) {
    this.distanceControl = distanceControl;
  }

  /**
   * @param esum
   *          the esum to set
   */
  public void setEsum(int esum) {
    this.esum = esum;
  }

  /**
   * setter for the front X position
   * 
   * @param frontX
   *          the front y position
   */
  private void setFrontX(int frontX) {
    this.frontX = frontX;
  }

  /**
   * setter for the front Y position
   * 
   * @param frontY
   *          the front Y position
   */
  private void setFrontY(int frontY) {
    this.frontY = frontY;
  }

  /**
   * setter for jostle
   * 
   * @param jostle
   */
  public void setJostle(boolean jostle) {
    this.jostle = jostle;
  }

  /**
   * sets the middle x value
   * 
   * @param middleX
   */
  private void setMiddleX(int middleX) {
    this.middleX = middleX;
  }

  /**
   * setter for the middle Y value
   * 
   * @param middleY
   */
  private void setMiddleY(int middleY) {
    this.middleY = middleY;
  }

  /**
   * @param movement
   *          the movement to set
   */
  public void setMovement(boolean movement) {
    this.movement = movement;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * @param oldcelerity
   *          the oldcelerity to set
   */
  public void setOldcelerity(int oldcelerity) {
    this.oldcelerity = oldcelerity;
  }

  /**
   * @param oldSollabstandValue
   *          the oldSollabstandValue to set
   */
  public void setOldSollabstandValue(int oldSollabstandValue) {
    this.oldSollabstandValue = oldSollabstandValue;
  }

  /**
   * setter function for the PosX
   * 
   * @param posX
   *          the new x-position
   */
  public void setPosX(int posX) {
    this.posX = posX;
    updatePositions();
  }

  /**
   * setter function for the PosY
   * 
   * @param posY
   *          the new y-position
   */
  public void setPosY(int posY) {
    this.posY = posY;
    updatePositions();
  }

  /**
   * getter if the car is to near
   * 
   * @param toNear
   */
  public void setToNear(boolean toNear) {
    this.toNear = toNear;
  }

  public void switchDistanceControl() {
    switch (distanceControl) {
    case ON:
      disabledistanceControl();
      break;
    case OFF:
      enabledistanceControl();
      break;
    }
  }

  /**
   * updates the koordinate box from the car
   */
  public void updatePositions() {
    setFrontX(getPosX());
    setFrontY(getPosY());
    setMiddleX(getPosX() + 32);
    setMiddleY(getPosY() + 16);
    setBackX(getPosX() + 64);
    setBackY(getPosY() + 32);
  }
}