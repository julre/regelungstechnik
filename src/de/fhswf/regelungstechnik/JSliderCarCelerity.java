/**
 * 
 */
package de.fhswf.regelungstechnik;

import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.fhswf.regelungstechnik.model.AbstandsregelungModel;

/**
 * Class for the celerity slider.
 * 
 * @author Julian Rehborn
 */
public class JSliderCarCelerity extends JSlider implements ChangeListener {

  private static final long serialVersionUID = 1L;

  /**
   * Model of the program.
   */
  private AbstandsregelungModel model;

  /**
   * Sets the model, max/min values and the behavior of the slider.
   * 
   * @param model
   *          the model of the program.
   */
  public JSliderCarCelerity(AbstandsregelungModel model) {
    super(SwingConstants.VERTICAL);
    setMaximum(90);
    setMinimum(30);
    this.model = model;

    // prevent wrong celerity values.
    if (model.getBackCar().getCelerity() < getMinimum())
      model.getBackCar().setCelerity(getMinimum());
    if (model.getBackCar().getCelerity() > getMaximum())
      model.getBackCar().setCelerity(getMaximum());

    setValue(model.getBackCar().getCelerity());
    setMinorTickSpacing(1);
    setMajorTickSpacing(10);
    setPaintTicks(true);
    setPaintLabels(true);
    addChangeListener(this);
  }

  /**
   * Sets the Celerity for the regulation..
   */
  @Override
  public void stateChanged(ChangeEvent e) {
    final JSlider src = (JSlider) e.getSource();
    // if (!src.getValueIsAdjusting()) {
    model.getBackCar().setCelerity(src.getValue());
    model.getFrontCar().setCelerity(src.getValue());
    // }
  }
}
