package de.fhswf.regelungstechnik;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.JPanel;

import de.fhswf.regelungstechnik.Regulator.Status;
import de.fhswf.regelungstechnik.model.AbstandsregelungModel;
import de.fhswf.regelungstechnik.panels.JPanelCelerityControl;
import de.fhswf.regelungstechnik.panels.JPanelControls;
import de.fhswf.regelungstechnik.panels.JPanelVisualisation;

/**
 * @author Lukas Kopecki
 * @author Julian Rehborn
 * @author Andreas Michel Main class of this program. Creates the applet, the
 *         thread and does all graphical work.
 */
public class Main extends JApplet implements Runnable {

  private static final long serialVersionUID = 1L;

  /**
   * The model. Contains all data.
   */
  private AbstandsregelungModel model;

  /**
   * Panal for the top-buttons.
   */
  private JPanel mainControls;

  /**
   * Panal for the celerity-slider on the right side.
   */
  private JPanel celerityControl;

  /**
   * Panel for the center.
   */
  private JPanelVisualisation visu;

  /**
   * Thread for parallel calculations and visualisations.
   */
  private Thread runner;

  /**
   * The regulator.
   */
  private Regulator regulator;

  /**
   * Default constructor.
   */
  public Main() {
  }

  @Override
  public void destroy() {
    super.destroy();
  }

  /**
   * @return the regulator
   */
  public Regulator getRegulator() {
    return regulator;
  }

  @Override
  public void init() {
    super.init();
    setSize(755, 720);

    // Initialize the attibutes.
    model = new AbstandsregelungModel();
    regulator = new Regulator();
    mainControls = new JPanelControls(model);
    celerityControl = new JPanelCelerityControl(model);

    // sets layout.
    setLayout(new BorderLayout());
    add(celerityControl, BorderLayout.EAST);
    add(mainControls, BorderLayout.NORTH);

    // Initialize the attibutes.

    model = new AbstandsregelungModel();
    regulator = new Regulator();
    visu = new JPanelVisualisation(model, this);
    mainControls = new JPanelControls(model);
    celerityControl = new JPanelCelerityControl(model);

    setLayout(new BorderLayout());
    add(celerityControl, BorderLayout.EAST);
    add(mainControls, BorderLayout.NORTH);
    add(visu, BorderLayout.CENTER);

  }

  @Override
  public void run() {

    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    while (runner != null) {

      // move car.
      if (model.getDoRun()) {
        regulator.move(model.getFrontCar().getBackX(),
            model.getReferenceDistance() * model.getMeterScalingfactor(),
            model.getBackCar());

        // move trees.
        visu.moveTrees();

      }

      // sets status for color-visualisation for the controllloop.
      regulator.setStatus(Status.OFF);
      if (model.getBackCar().isDistanceControlON()) {
        if (-model.getCurrentDistance() / model.getMeterScalingfactor() + 1 < model
            .getReferenceDistance())
          regulator.setStatus(Status.BRAKE);
        if (-model.getCurrentDistance() / model.getMeterScalingfactor() > model
            .getReferenceDistance())
          regulator.setStatus(Status.SPEEDUP);
      }
      // repaint graphics an gui.
      visu.repaint();
      mainControls.repaint();
      celerityControl.repaint();

      repaint();

      // wait 50 ms.
      try {
        Thread.sleep(25);
      } catch (InterruptedException e) {
        e.printStackTrace();
        // do nothing
      }

    }

    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
  }

  @Override
  public void start() {
    System.out.println("start");
    if (runner == null) {
      runner = new Thread(this);
      runner.start();
    }
    super.start();
  }

  @Override
  public void stop() {
    System.out.println("stop");
    if (runner != null && runner.isAlive())
      runner = null;

    runner = null;
    super.stop();
  }

}
