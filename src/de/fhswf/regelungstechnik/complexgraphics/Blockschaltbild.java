package de.fhswf.regelungstechnik.complexgraphics;

import java.awt.Color;
import java.awt.Graphics;

import de.fhswf.regelungstechnik.Regulator;
import de.fhswf.regelungstechnik.Regulator.Status;

/**
 * draws the BlockschaltBild
 * 
 * @author Lukas Kopecki
 * @author Andreas Michel
 */
public class Blockschaltbild extends Blockbild {

  /**
   * draws the Blockschaltbild
   * 
   * @param g
   *          Grpahics interface
   * @param celerity
   *          the Speed from the car
   * @param istabstand
   *          the istabstand from the secondcar
   * @param sollabstand
   *          the sollabstand from the secondcar
   * @param controll
   *          how the distance is controlled
   * @param status
   *          the status from the cars.
   */
  public void paint(Graphics g, int celerity, int istabstand, int sollabstand,
      boolean controll, Regulator.Status status, int yOffset) {
    g.setColor(new Color(238, 238, 238));
    g.fillRect(0, 0 + yOffset, 713, 300);
    g.setColor(new Color(0, 0, 0));

    // draw Input "eigenes Fahrzeug"
    g.drawString("eigenes Fahrzeug", 180, 10 + yOffset);

    // draw Arrow
    g.drawString("4", 210, 30 + yOffset);
    drawArrowVertical(g, 225, 20 + yOffset, 225, 40 + yOffset);
    drawArrowVertical(g, 225, 85 + yOffset, 225, 115 + yOffset);

    // draw Mikrowellen radar
    drawTextRect(g, "Mikrowellen-", "radar", 175, 40 + yOffset);
    g.drawString(istabstand + " m", 225, 70 + yOffset);

    // draw Input "vorausfahrendes Fahrzeug"
    g.drawString("vorrausfahrendes Fahrzeug", 330, 10 + yOffset);
    // draw Arrow
    g.drawString("3", 385, 30 + yOffset);
    drawArrowVertical(g, 400, 20 + yOffset, 400, 40 + yOffset);
    drawArrowVertical(g, 400, 85 + yOffset, 400, 115 + yOffset);

    // draw Tachometer
    drawTextRect(g, "Tachometer", "", 350, 40 + yOffset);
    g.drawString(celerity + " km/h", 365, 70 + yOffset);

    // draw Arrow Eingabegerät
    drawArrowHorizontal(g, 100, 135 + yOffset, 175, 135 + yOffset);
    g.drawString("5", 125, 130 + yOffset);

    // draw Eingabegerät
    drawTextRect(g, "Eingabe-", "geraet", 0, 115 + yOffset);

    if (controll)
      g.drawString("automatic", 15, 157 + yOffset);
    else
      g.drawString("manuell", 15, 157 + yOffset);

    // draw Arrow Anzeigegeräte
    drawArrowHorizontal(g, 100, 210 + yOffset, 175, 210 + yOffset);
    g.drawString("6", 125, 205 + yOffset);

    // draw anzeigegeräte
    drawTextRect(g, "Anzeige-", "geraete", 0, 190 + yOffset);
    g.drawString(sollabstand + " m", 15, 232 + yOffset);

    // draw Arrow Antrieb
    drawArrowHorizontal(g, 450, 135 + yOffset, 525, 135 + yOffset);
    g.drawString("1", 655, 130 + yOffset);
    drawArrowHorizontal(g, 625, 135 + yOffset, 690, 135 + yOffset);

    // draw Antrieb
    if (status == Status.SPEEDUP)
      g.setColor(new Color(155, 0, 0));
    drawTextRect(g, "Antrieb", "", 525, 115 + yOffset);
    g.setColor(new Color(0, 0, 0));

    // draw Arrow Bremse
    drawArrowHorizontal(g, 450, 210 + yOffset, 525, 210 + yOffset);
    g.drawString("2", 655, 205 + yOffset);
    drawArrowHorizontal(g, 625, 210 + yOffset, 690, 210 + yOffset);

    // draw Bremse
    if (status == Status.BRAKE)
      g.setColor(new Color(0, 0, 255));
    drawTextRect(g, "Bremse", "", 525, 190 + yOffset);
    g.setColor(new Color(0, 0, 0));

    // draw Mikrorechner
    g.drawString("Mikrorechner", 275, 175 + yOffset);
    g.drawRect(175, 115 + yOffset, 275, 120);

    if (controll)
      g.drawString("Berechne...", 275, 190 + yOffset);
    else
      g.drawString("tue nichts", 275, 190 + yOffset);

    // the help text
    g.drawString(
        "1 Beschleunigungskraefte, 2 Verzoegerungskraefte, 3 Fahrzeuggeschwindigkeit",
        0, 250 + yOffset);
    g.drawString("4 Abstand, 5 Vorgaben des Fahrers, 6 Anzeigen", 0,
        265 + yOffset);
  }
}
