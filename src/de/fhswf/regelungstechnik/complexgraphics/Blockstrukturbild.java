package de.fhswf.regelungstechnik.complexgraphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 * draws the BlockschaltBild
 * 
 * @author Lukas Kopecki
 * @author Andreas Michel
 */
public class Blockstrukturbild extends Blockbild {

  /**
   * paints the Blockstrukturbild on the graphic device
   * 
   * @param g
   *          the graphic device
   * @param regelung
   *          the automatic control of the program
   * @param carRegelung
   *          automatic control of the car
   * @param speed
   *          the speed from the car
   * @param istabstand
   *          the istabstand from the secondcar
   * @param sollabstand
   *          the sollabstand from the secondcar
   */
  public void paint(Graphics g, boolean regelung, boolean carRegelung,
      int speed, int istabstand, int sollabstand, int yOffset) {
    g.setColor(new Color(238, 238, 238));
    g.fillRect(0, 0 + yOffset, 713, 300);
    g.setColor(new Color(0, 0, 0));

    // input arrow
    g.setColor(new Color(0, 0, 0));
    drawArrowHorizontal(g, 0, 37 + yOffset, 15, 37 + yOffset);
    g.drawString("10", 0, 32 + yOffset);

    // Draw Abstandsregler
    drawTextRect(g, "Abstands-", "regler", 15, 15 + yOffset);
    // draw arrow
    drawArrowHorizontal(g, 115, 37 + yOffset, 145, 37 + yOffset);

    // draw Geschwindigkeitsregler
    drawTextRect(g, "Geschwindig-", "keitsregler", 145, 15 + yOffset);
    g.drawString("8", 130, 32 + yOffset);
    // draw arrow
    drawArrowHorizontal(g, 245, 37 + yOffset, 275, 37 + yOffset);

    // draw Stellorgane
    drawTextRect(g, "Stellglieder", "", 275, 15 + yOffset);
    if (istabstand + 1 < sollabstand && carRegelung) {
      g.setColor(new Color(0, 0, 255));
      g.drawString("Bremsen", 290, 45 + yOffset);
      g.setColor(new Color(0, 0, 0));
    }
    if (istabstand > sollabstand && carRegelung) {
      g.setColor(new Color(155, 0, 0));
      g.drawString("Beschleunigen", 290, 45 + yOffset);
      g.setColor(new Color(0, 0, 0));
    }
    g.drawString("6", 260, 32 + yOffset);
    // draw arrow
    drawArrowHorizontal(g, 375, 37 + yOffset, 405, 37 + yOffset);

    // draw Fahrzeugdynamik
    drawTextRect(g, "Fahrzeug-", "dynamik", 405, 15 + yOffset);
    g.drawString("5", 390, 32 + yOffset);
    // draw arrow
    drawArrowHorizontal(g, 505, 37 + yOffset, 535, 37 + yOffset);
    g.drawString("4", 460, 10 + yOffset);
    drawArrowVertical(g, 450, 0 + yOffset, 450, 15 + yOffset);

    // draw Kinematik
    drawTextRect(g, "Kinematik", "", 535, 15 + yOffset);
    g.drawString("3", 513, 32 + yOffset);
    g.drawString("2", 595, 10 + yOffset);
    drawArrowVertical(g, 585, 0 + yOffset, 585, 15 + yOffset);

    // draw arrow ausgabe
    drawArrowHorizontal(g, 635, 37 + yOffset, 700, 37 + yOffset);
    g.drawString("1", 673, 32 + yOffset);

    if (regelung) {
      if (carRegelung) {
        if (istabstand + 1 < sollabstand)
          g.setColor(new Color(0, 0, 255));
        if (istabstand > sollabstand)
          g.setColor(new Color(155, 0, 0));
      } else
        g.setColor(new Color(0, 0, 0));

    } else
      g.setColor(new Color(0, 0, 0));

    // draw Tachometer
    drawTextRect(g, "Tachometer", "", 275, 90 + yOffset);
    g.drawString(speed + " km/h", 290, 120 + yOffset);
    g.drawString("7", 260, 105 + yOffset);
    g.drawString("3", 390, 102 + yOffset);

    // draw Mikrowellenradar
    drawTextRect(g, "Mikrowellen-", "radar", 275, 175 + yOffset);
    g.drawString(istabstand + " m", 290, 218 + yOffset);
    g.drawString("9", 260, 190 + yOffset);
    g.drawString("1", 390, 190 + yOffset);

    // arrow from kinematik to mikrowellenradar
    g.drawLine(674, 37 + yOffset, 674, 197 + yOffset);
    g.drawLine(675, 37 + yOffset, 675, 197 + yOffset);
    g.drawLine(676, 37 + yOffset, 676, 197 + yOffset);
    g.drawOval(673, 35 + yOffset, 5, 5);
    g.drawLine(675, 197 + yOffset, 375, 197 + yOffset);
    g.drawLine(675, 196 + yOffset, 375, 196 + yOffset);
    g.drawLine(675, 195 + yOffset, 375, 195 + yOffset);
    g.drawLine(375, 196 + yOffset, 390, 202 + yOffset);
    g.drawLine(375, 196 + yOffset, 390, 190 + yOffset);

    // arrow from mikrowellenradar to abstandsradar
    g.drawLine(275, 196 + yOffset, 65, 196 + yOffset);
    g.drawLine(275, 197 + yOffset, 65, 197 + yOffset);
    g.drawLine(275, 198 + yOffset, 65, 198 + yOffset);
    g.drawLine(64, 197 + yOffset, 64, 60 + yOffset);
    g.drawLine(65, 197 + yOffset, 65, 60 + yOffset);
    g.drawLine(66, 197 + yOffset, 66, 60 + yOffset);
    g.drawLine(65, 60 + yOffset, 55, 70 + yOffset);
    g.drawLine(65, 60 + yOffset, 75, 70 + yOffset);

    // arrow from fahrzeugdynamik to tachometer
    g.drawLine(514, 37 + yOffset, 514, 112 + yOffset);
    g.drawLine(515, 37 + yOffset, 515, 112 + yOffset);
    g.drawLine(516, 37 + yOffset, 516, 112 + yOffset);
    g.drawOval(513, 35 + yOffset, 5, 5);
    g.drawLine(515, 111 + yOffset, 375, 111 + yOffset);
    g.drawLine(515, 112 + yOffset, 375, 112 + yOffset);
    g.drawLine(515, 113 + yOffset, 375, 113 + yOffset);
    g.drawLine(375, 112 + yOffset, 390, 117 + yOffset);
    g.drawLine(375, 112 + yOffset, 390, 107 + yOffset);

    // arrow from tachometer to geschwindigkeitsregler
    g.drawLine(275, 111 + yOffset, 195, 111 + yOffset);
    g.drawLine(275, 112 + yOffset, 195, 112 + yOffset);
    g.drawLine(275, 113 + yOffset, 195, 113 + yOffset);
    g.drawLine(194, 112 + yOffset, 194, 60 + yOffset);
    g.drawLine(195, 112 + yOffset, 195, 60 + yOffset);
    g.drawLine(196, 112 + yOffset, 196, 60 + yOffset);
    g.drawLine(195, 60 + yOffset, 185, 70 + yOffset);
    g.drawLine(195, 60 + yOffset, 205, 70 + yOffset);

    // draw the help text
    g.setColor(new Color(0, 0, 0));
    g.drawString(
        "1 Abstand, 2 Geschwindigkeit des vorausfahrenden Fahrzeugs, 3 Geschwindigkeit des eigenes Fahrzeugs, 4 Stoerkraefte",
        0, 250 + yOffset);
    g.drawString(
        "5 Beschleunigung, 6 Reglerausgangsgroesse, 7 Fahrzeuggeschwindigkeit, 8 Sollgeschwindigkeit, 9 Abstand, 10 Sollabstand",
        0, 265 + yOffset);
  }
}
