package de.fhswf.regelungstechnik.complexgraphics;

import java.awt.Graphics;

/**
 * main class for drawing Blockbilder. Provides functions for drawing arrows and
 * textRects for Visualition
 * 
 * @author Lukas Kopecki
 */
public class Blockbild {

  /**
   * draws an horizontal Arrow.
   * 
   * @param g
   *          the Graphics Interface
   * @param startX
   *          the Start X position of the Arrow
   * @param startY
   *          the Start Y position of the Arrow
   * @param endX
   *          the End X position of the Arrow
   * @param endY
   *          the End Y position of the Arrow
   */
  protected void drawArrowHorizontal(Graphics g, int startX, int startY,
      int endX, int endY) {
    g.drawLine(startX, startY - 1, endX, endY - 1);
    g.drawLine(startX, startY, endX, endY);
    g.drawLine(startX, startY + 1, endX, endY + 1);
    g.drawLine(endX, endY, endX - 15, endY - 5);
    g.drawLine(endX, endY, endX - 15, endY + 5);
  }

  /**
   * draws an vertical arrow
   * 
   * @param g
   *          the Graphics Interface
   * @param startX
   *          the Start X position of the Arrow
   * @param startY
   *          the Start Y position of the Arrow
   * @param endX
   *          the End X position of the Arrow
   * @param endY
   *          the End Y position of the Arrow
   */
  protected void drawArrowVertical(Graphics g, int startX, int startY,
      int endX, int endY) {
    g.drawLine(startX - 1, startY, endX - 1, endY);
    g.drawLine(startX, startY, endX, endY);
    g.drawLine(startX + 1, startY, endX + 1, endY);
    g.drawLine(endX, endY, endX - 5, endY - 10);
    g.drawLine(endX, endY, endX + 5, endY - 10);
  }

  /**
   * draws a Rect with two Strings.
   * 
   * @param g
   *          Graphics interface
   * @param text
   *          first text
   * @param text2
   *          second text, will be under the first one
   * @param posX
   *          the x - position of the box
   * @param posY
   *          the y - position of the box
   */
  protected void drawTextRect(Graphics g, String text, String text2, int posX,
      int posY) {
    g.drawString(text, posX + 15, posY + 15);
    g.drawString(text2, posX + 15, posY + 30);
    g.drawRect(posX, posY, 100, 45);
  }
}
