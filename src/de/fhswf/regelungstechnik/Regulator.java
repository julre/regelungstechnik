/**
 * 
 */
package de.fhswf.regelungstechnik;

import de.fhswf.regelungstechnik.model.Car;

/**
 * @author Julian Rehborn
 * @author Andreas Michel
 */
public class Regulator {

  public enum Status {
    OFF, SPEEDUP, BRAKE
  }

  private Status status;

  public Regulator() {
    status = Status.OFF;
  }

  /**
   * @return the status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Calculates the behavior of the backCar.
   * Moves the car in a frequenz of 0.5sec on the sinusfunction, at the
   * referenceDistance if the regulator is off.
   * 
   * @param currentDistance
   *          the current distance between the two cars.
   * @param referenceDistance
   *          the reference distance.
   * @param car
   *          the car which should be controlled (backCar)
   */
  public void move(int currentDistance, int referenceDistance, Car car) {

    if (!car.isDistanceControlON()) {
      // distance controll off
      if (car.isMovement() != true)
        if (currentDistance + referenceDistance < car.getFrontX() - 1)
          car.setPosX(car.getPosX() - 1);
        else if (currentDistance + referenceDistance > car.getFrontX() + 1)
          car.setPosX(car.getPosX() + 1);
        else {
          car.setMovement(true);
          car.setAnimationCounter(0);
        }

      if (car.isMovement() == true) {
        car.setPosX(currentDistance
            + (int) (referenceDistance + Math.sin(car.getAnimationCounter() / 2
                * Math.PI / 180)
                * referenceDistance * 0.05));
        car.setAnimationCounter(car.getAnimationCounter() + 1);
      }

      if (car.getOldSollabstandValue() != referenceDistance)

        car.setMovement(false);
      car.setOldSollabstandValue(referenceDistance);
    } else {
      // Distance-controll on

      car.setMovement(false);

      if (car.getOldcelerity() != car.getCelerity())
        car.setEsum(0);

      car.setOldcelerity(car.getCelerity());

      // I - Glied
      car.setEsum(car.getEsum() + car.getFrontX() - referenceDistance
          - currentDistance);
      if (car.getFrontX() - referenceDistance - currentDistance < -4
          || car.getFrontX() - referenceDistance - currentDistance > 4) {
        car.setPosX(car.getFrontX() - (int) (0.0002 * car.getEsum()));

        car.enabledistanceControl();
      }

    }

    car.updatePositions();
  }

  /**
   * @param status
   *          the status to set
   */
  public void setStatus(Status status) {
    this.status = status;
  }

}
